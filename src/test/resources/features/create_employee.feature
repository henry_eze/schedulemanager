@Employee
Feature: Create employee

  Scenario: create employee successfully with valid details
    Given employee details, such as first name, last name, sex are provided
    When a new employee is created with the provided details
    Then the details of the newly created employee must be the same as the one that was provided
    And a unique number is assigned to the employee for identification