package schedule;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Ositadinma on 12/08/2016.
 */
@Configuration
@ComponentScan(basePackages = "schedule")
public class CucumberConfiguration {
}
