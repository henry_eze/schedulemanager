package schedule;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
		features = {"src/test/resources/features"},
		glue = {"schedule.step_definitions"},
        snippets = SnippetType.CAMELCASE

)
public class ScheduleManagerApplicationTest{

}