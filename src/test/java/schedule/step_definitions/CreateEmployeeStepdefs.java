package schedule.step_definitions;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import schedule.CucumberConfiguration;
import schedule.model.Employee;
import schedule.model.Role;
import schedule.model.Sex;
import schedule.repository.EmployeeRepository;

import static org.junit.Assert.*;

@ContextConfiguration(classes = {CucumberConfiguration.class})
public class CreateEmployeeStepdefs{

    private String firstName;
    private String lastName;
    private Role role;
    private Sex sex;

    @Autowired
    private EmployeeRepository employeeRepository;

    Employee employee;
    Employee savedEmployee;


    @Given("^employee details, such as first name, last name, sex are provided$")
    public void provideEmployeeDetails() throws Throwable {
        firstName="Henry";
        lastName ="Eze";
        sex= Sex.MALE;
        role = Role.JUNIOR_SUPPORT_WORKER;
    }

    @When("^a new employee is created with the provided details$")
    public void aNewEmployeeIsCreatedWithTheProvidedDetails() throws Throwable {
        employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmployeeRole(role);
        employee.setSex(sex);
        savedEmployee  = employeeRepository.save(employee);

    }

    @Then("^the details of the newly created employee must be the same as the one that was provided$")
    public void theDetailsOfTheNewlyCreatedEmployeeMustBeTheSameAsTheOneThatWasProvided() throws Throwable {
        assertEquals(savedEmployee.getFirstName(),employee.getFirstName());
        assertEquals(savedEmployee.getLastName(),employee.getLastName());
        assertEquals(savedEmployee.getSex(),employee.getSex());
        assertEquals(savedEmployee.getEmployeeRole(),employee.getEmployeeRole());
    }

    @Then("^a unique number is assigned to the employee for identification$")
    public void aUniqueNumberIsAssignedToTheEmployeeForIdentification() throws Throwable {
        assertNotNull(savedEmployee.getId());
    }
}