package schedule;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.h2.server.web.WebServlet;
import schedule.model.Employee;
import schedule.model.Role;
import schedule.model.Sex;
import schedule.repository.EmployeeRepository;
import schedule.service.EmployeeService;

@SpringBootApplication
public class ScheduleManagerApplication {


	public static void main(String[] args) {


		SpringApplication.run(ScheduleManagerApplication.class, args);
	}


	/*@Bean
	ServletRegistrationBean h2servletRegistration(){
		ServletRegistrationBean registrationBean = new ServletRegistrationBean( new WebServlet());
		registrationBean.addUrlMappings("/console*//*");
		return registrationBean;
	}*/


		@Bean
		public CommandLineRunner runApp(EmployeeRepository repository){

			return (args)->{
				repository.save(new Employee("Henry","Ositadinma","Eze", Sex.MALE, Role.MANAGER));
                repository.save(new Employee("Chikadibia", "Nwnado","Obi",Sex.FEMALE, Role.SENIOR_SUPPORT_WORKER));
			};

		}
}
