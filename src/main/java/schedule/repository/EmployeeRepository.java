package schedule.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import schedule.model.Employee;

/**
 * Created by Ositadinma on 13/08/2016.
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {



}