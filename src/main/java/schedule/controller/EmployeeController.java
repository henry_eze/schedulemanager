package schedule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import schedule.model.Employee;
import schedule.service.EmployeeService;

import java.util.List;

/**
 * Created by Ositadinma on 09/08/2016.
 */


@RestController
@RequestMapping("/employees")
public class EmployeeController {


    @Autowired
    private EmployeeService employeeService;

    @RequestMapping
    public List<Employee> getEmployees(){
        return employeeService.getAllEmployees();
    }

}
