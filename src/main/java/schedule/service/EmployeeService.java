package schedule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import schedule.model.Employee;
import schedule.repository.EmployeeRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ositadinmaeze on 07/08/2016.
 */

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees(){


        List<Employee> employees = new ArrayList<>();

        Iterable<Employee> iterable = employeeRepository.findAll();
        iterable.forEach(employees::add);
        return employees;
    }

    public Employee saveEmployee(Employee employee){

        Employee savedEmployee =employeeRepository.save(employee);
        return savedEmployee;
    }
}
