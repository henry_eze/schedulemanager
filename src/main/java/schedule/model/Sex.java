package schedule.model;

/**
 * Created by Ositadinma on 13/08/2016.
 */
public enum Sex {
    MALE, FEMALE
}
