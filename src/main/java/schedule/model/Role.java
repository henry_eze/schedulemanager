package schedule.model;

/**
 * Created by Ositadinma on 13/08/2016.
 */
public enum Role {
    JUNIOR_SUPPORT_WORKER, SENIOR_SUPPORT_WORKER, MANAGER, DEPUTY_MANAGER
}
