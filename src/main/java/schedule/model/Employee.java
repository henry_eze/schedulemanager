package schedule.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ositadinmaeze on 12/06/2016.
 */
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Integer version;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Enumerated(EnumType.STRING)
    private Role employeeRole;

    /*@ElementCollection
    @CollectionTable(name="employee_skill_types", joinColumns=@JoinColumn(name="id"))
    @Column(name = "skill_type")
    private Set<String> skillType = new HashSet();*/


    public Employee(){}

    public Employee(String firstName, String middleName, String lastName, Sex sex, Role employeeRole){
        this.setFirstName(firstName);
        this.setMiddleName(middleName);
        this.setLastName(lastName);
        this.setSex(sex);
        this.setEmployeeRole(employeeRole);

    }

    public Employee(String firstName, String lastName, Sex sex, Role employeeRole){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setSex(sex);
        this.setEmployeeRole(employeeRole);

    }

    public Employee(String firstName, String lastName){
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Role getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(Role employeeRole) {
        this.employeeRole = employeeRole;
    }

   /* public Set<String> getSkillType() {
        return skillType;
    }

    public void setSkillType(Set<String> skillType) {
        this.skillType = skillType;
    }
*/
    public Integer getVersion() {
        return version;
    }

    protected void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString(){
        return this.getFirstName() + " " + this.getLastName();
    }
}
