package schedule.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by Ositadinma on 15/06/2016.
 */
@Entity
@Table(name="training_schedule")
public class TrainingSchedule extends Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "address")
    private String address;
    @Column(name = "lecturer_first_name")
    private String lecturerFirstName;
    @Column(name = "lecturer_last_name")
    private String lecturerLastName;
    @Column
    private LocalDate date;

    public TrainingSchedule(){}

    public TrainingSchedule(LocalTime startTime, LocalTime endTime, String address, String lecturerFirstName, String lecturerLastName){
        this.setStartTime(startTime);
        this.setEndTime(endTime);
        this.setAddress(address);
        this.setLecturerFirstName(lecturerFirstName);
        this.setLecturerLastName(lecturerLastName);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLecturerFirstName() {
        return lecturerFirstName;
    }

    public void setLecturerFirstName(String lecturerFirstName) {
        this.lecturerFirstName = lecturerFirstName;
    }

    public String getLecturerLastName() {
        return lecturerLastName;
    }

    public void setLecturerLastName(String lecturerLastName) {
        this.lecturerLastName = lecturerLastName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
